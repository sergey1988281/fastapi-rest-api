from datetime import date
from typing import List, Union
from sqlalchemy.orm import Session
from fastapi import (Depends,
                     HTTPException,
                     status)
from ..models.operation import (Operation as Operation_model,
                                BaseOperation,
                                OperationCreate,
                                OperationUpdate,
                                OperationKind)
from ..models.orm.operation import Operation as Operation_table
from ..database import get_session, commit_to_session, add_to_session
from ..log import logger


class OperationService:
    def __init__(self,
                 session: Session = Depends(get_session)
                 ) -> None:
        """Initializes operations service
        Args:
            session (Session, optional): SQL Alchemy session object.
            Defaults to Depends(get_session).
        """
        self.session = session

    def get_operation(self,
                      id: int,
                      user_id: int
                      ) -> Operation_table:
        """Retrieves specific operation by id
        Args:
            id (int): id of post to retrieve.
            user_id (int): limit to user with specific id
        Returns:
            Operation_table: retrieved post
        Raises:
            SQLAlchemy exceptions
            HTTP exceptions: 404
        """
        try:
            operation = self.session.query(Operation_table) \
                .filter_by(id=id, user_id=user_id) \
                .first()
        except Exception as err:
            logger.exception(str(err))
            raise
        if not operation:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return operation

    def get_operations(self,
                       user_id: int,
                       kind: Union[None, OperationKind] = None,
                       start_date: Union[None, date] = None,
                       end_date: Union[None, date] = None,
                       ) -> List[Operation_model]:
        """Retrieves list of operations given filters
        Args:
            user_id (int): limit to user with specific id
            kind (Optional[OperationKind], optional): Kind of operation:
            income or outcome. Defaults to None.
            start_date (Optional[date], optional): From which date inclusive.
            Defaults to None.
            end_date (Optional[date], optional): Till which date inclusive.
            Defaults to None.
        Returns:
            List[Operation_model]: List of operations, may be empty
        """
        query = self.session.query(Operation_table).filter_by(user_id=user_id)
        if kind:
            query = query.filter_by(kind=kind)
        if start_date:
            query = query.filter(Operation_table.date >= start_date)
        if end_date:
            query = query.filter(Operation_table.date <= end_date)
        try:
            operations = query.all()
        except Exception as err:
            logger.exception(str(err))
            raise
        return operations

    def create_operation(self,
                         user_id: int,
                         operation_data: OperationCreate
                         ) -> Operation_table:
        """Creates a new operation
        Args:
            user_id (int): limit to user with specific id
            operation_data (OperationCreate): Details for operation to create
        Returns:
            Operation_table: Created operation with id
        Raises:
            SQLAlchemy exceptions
        """
        operation = Operation_table(**operation_data.dict(), user_id=user_id)
        add_to_session(self.session, operation)
        commit_to_session(self.session)
        return operation

    def create_bulk(self,
                    user_id: int,
                    operations_data: List[OperationCreate]
                    ) -> List[Operation_table]:
        operations = [Operation_table(**operation_data.dict(),
                                      user_id=user_id)
                      for operation_data in operations_data]
        add_to_session(self.session, operations)
        commit_to_session(self.session)
        return operations

    def update_operation(self,
                         id: int,
                         user_id: int,
                         operation_data: OperationUpdate
                         ) -> Operation_table:
        """Updates existing operation data
        Args:
            id (int): Id of operation to update
            user_id (int): limit to user with specific id
            operation_data (OperationUpdate): Details of operation to update
        Returns:
            Operation_table: Updated operation data
        Raises:
            SQL Alchemy exception
        """
        operation = self.get_operation(id=id, user_id=user_id)
        for field, value in operation_data:
            setattr(operation, field, value)
        commit_to_session(self.session)
        return operation

    def delete_operation(self,
                         id: int,
                         user_id: int) -> None:
        """Deletes operation by provided id
        Args:
            id (int): Id of operation to delete
            user_id (int): limit to user with specific id
        Raises:
            SQL Alchemy exception
        """
        operation = self.get_operation(id=id, user_id=user_id)
        try:
            self.session.delete(operation)
        except Exception as err:
            logger.exception(str(err))
            raise
        commit_to_session(self.session)

    def update_operation_description(self,
                                     id: int,
                                     user_id: int,
                                     operation_data: BaseOperation,
                                     ) -> Operation_table:
        """Updates just post description
        Args:
            id (int): Id of post to update
            user_id (int): limit to user with specific id
            operation_data (ParentOperation): Contains only description
        Returns:
            Operation_table: Updted post
        """
        operation = self.get_operation(id=id, user_id=user_id)
        operation.description = operation_data.description
        commit_to_session(self.session)
        return operation
