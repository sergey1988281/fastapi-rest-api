import bcrypt
import jwt
from jwt.exceptions import InvalidTokenError
from datetime import datetime, timedelta
from fastapi import (Depends,
                     HTTPException,
                     status)
from fastapi.security import OAuth2PasswordBearer
from pydantic import ValidationError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from ..database import get_session, commit_to_session, add_to_session
from ..log import logger
from ..models.auth import User, Token, UserCreate
from ..models.orm.user import User as User_table
from ..settings import settings


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/sign-in/")


def get_current_user(token: str = Depends(oauth2_scheme)) -> User:
    return AuthService.validate_token(token)


class AuthService:
    @staticmethod
    def verify_password(plain_password: str, hashed_password: str) -> bool:
        """Verifies password using bsrypt algorithm
        Args:
            plain_password (str): Password
            hashed_password (str): Password proposed hash
        Returns:
            bool: Return either password matches hash or not
        """
        return bcrypt.checkpw(plain_password.encode("utf-8"),
                              hashed_password.encode('utf-8'))

    @staticmethod
    def hash_password(plain_password: str) -> str:
        """Generates hash out of password using bcrypt algorithm
        Args:
            plain_password (str): Password
        Returns:
            str: Hash of the password
        """
        return bcrypt.hashpw(
            plain_password.encode('utf-8'),
            bcrypt.gensalt()).decode("utf-8")

    @staticmethod
    def validate_token(token: str) -> User:
        """Validates token and extracts payload out
        of it in a form of User object
        Args:
            token (str): Token sring
        Raises:
            exception: HTTPException
        Returns:
            User: User object from payload
        """
        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token is invalid",
            headers={
                "WWW-Authentificate": "Bearer"
            }
        )
        try:
            payload = jwt.decode(token,
                                 settings.jwt_secret,
                                 algorithms=[settings.jwt_algorithm])
        except InvalidTokenError as err:
            logger.exception("Decode token error:" + str(err))
            raise exception
        user_data = payload.get("user")
        try:
            user = User.parse_obj(user_data)
        except ValidationError as err:
            logger.exception("Unable to parse object:" + str(err))
            raise exception
        return user

    @staticmethod
    def create_token(user: User_table) -> Token:
        """Generates JWT token out of user data
        Args:
            user_data (User_table): User details
        Raises:
            exception: PyJWKError
        Returns:
            Token: JWT token
        """
        user_data = User.from_orm(user)
        now = datetime.utcnow()
        payload = {
            "iat": now,
            "nbf": now,
            "exp": now + timedelta(seconds=settings.jwt_expiration),
            "sub": str(user_data.id),
            "user": user_data.dict()
        }
        try:
            token = jwt.encode(
                payload,
                settings.jwt_secret,
                algorithm=settings.jwt_algorithm
            )
        except InvalidTokenError as err:
            logger.exception("Encode token error:" + str(err))
            raise
        return Token(access_token=token)

    def __init__(self,
                 session: Session = Depends(get_session)
                 ) -> None:
        """Initializes authentifiction/authorization service
        Args:
            session (Session, optional): SQL Alchemy session object.
            Defaults to Depends(get_session).
        """
        self.session = session

    def register_user(self, user_data: UserCreate) -> User:
        """Registers a new user
        Args:
            user_data (UserCreate): Data of user to register
        Raises:
            exception: SQLAlchemy exception
        Returns:
            User: Data for created user
        """
        user = User_table(
            email=user_data.email,
            username=user_data.username,
            password_hash=self.hash_password(user_data.password)
        )
        add_to_session(self.session, user)
        try:
            commit_to_session(self.session)
        except IntegrityError:
            logger.exception("Existing username/email registration conflict")
            raise HTTPException(status_code=status.HTTP_409_CONFLICT)
        return User.from_orm(user)

    def authentificate_user(self, username: str, password: str) -> Token:
        """Authentificates user and return access token
        Args:
            username (str): provided username
            password (str): provided password
        Raises:
            exception: HTTPException
        Returns:
            Token: Valid token
        """
        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={
                "WWW-Authentificate": "Bearer"
            }
        )
        user = (
            self.session
            .query(User_table)
            .filter(User_table.username == username)
            .first()
        )
        if not user:
            logger.warning(f"Attempt to authentificate with invalid"
                           f" username {username}")
            raise exception
        if not self.verify_password(password, user.password_hash):
            logger.warning(f"Attempt to authentificate with invalid"
                           f" password for {username}")
            raise exception
        return self.create_token(user)

    def change_user_password(self,
                             user_id: int,
                             password: str) -> Token:
        """Changes password for user with specified id
        Args:
            user_id (int): ID of user to change password
            password (str): New pasword
        Returns:
            Token: Re-generated token
        """
        user = self.get_user(user_id)
        user.password_hash = self.hash_password(password)
        commit_to_session(self.session)
        return self.create_token(user)

    def delete_user(self,
                    user_id: int) -> None:
        """Deletes existing user from database
        Args:
            user_id (int): ID of user to delete
        Raises:
            HTTPException: In case user not found 404
        """
        user = self.get_user(user_id)
        try:
            self.session.delete(user)
        except Exception as err:
            logger.exception(str(err))
            raise
        commit_to_session(self.session)

    def get_user(self,
                 user_id: int
                 ) -> User_table:
        """Retrieves specific user by id
        Args:
            user_id (int): ID of user to retrieve
        Returns:
            User_table: retrieved user
        Raises:
            SQLAlchemy exceptions
            HTTP exceptions: 404
        """
        try:
            user = self.session.query(User_table).get(user_id)
        except Exception as err:
            logger.exception(str(err))
            raise
        if not user:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        return user
