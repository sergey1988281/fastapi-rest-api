from csv import DictReader, DictWriter
from datetime import date
from io import StringIO
from typing import Any, Union
from fastapi import Depends
from .operations import OperationService
from ..models.operation import Operation, OperationCreate, OperationKind
from ..log import logger


class ReportsService:
    def __init__(self,
                 operation_service: OperationService = Depends()
                 ) -> None:
        """Initializes reports service
        Args:
            operation_service (OperationService, optional): Operation service
            to read and write operations
            Defaults to Depends().
        """
        self.operation_service = operation_service

    def import_csv(self, user_id: int, file: Any) -> None:
        """Imports list of operations given list as file
        and specified user id
        Args:
            user_id (int): Id of user to import
            file (Any): File to import operations from
        """
        reader = DictReader(
            (line.decode("utf-8") for line in file),
            fieldnames=['date',
                        'kind',
                        'amount',
                        'description']
        )
        operations_data = []
        next(reader)
        for row in reader:
            operation_data = OperationCreate.parse_obj(row)
            if operation_data.description == "":
                operation_data.description = None
            operations_data.append(operation_data)
        self.operation_service.create_bulk(user_id=user_id,
                                           operations_data=operations_data)

    def export_csv(self,
                   user_id: int,
                   kind: Union[None, OperationKind] = None,
                   start_date: Union[None, date] = None,
                   end_date: Union[None, date] = None
                   ) -> Any:
        """Exports a file, containing list of operations
        for specific person
        Args:
            user_id (int): Id of user to export
            kind (Optional[OperationKind], optional): Kind of operation:
            income or outcome. Defaults to None.
            start_date (Optional[date], optional): From which date inclusive.
            Defaults to None.
            end_date (Optional[date], optional): Till which date inclusive.
            Defaults to None.
        Returns:
            Any: in-memory file like object, contaiing list of operations
        """
        output = StringIO()
        writer = DictWriter(
            output,
            fieldnames=['date',
                        'kind',
                        'amount',
                        'description'],
            extrasaction="ignore"
        )
        operations_data = self.operation_service.get_operations(
            user_id=user_id,
            kind=kind,
            start_date=start_date,
            end_date=end_date)
        writer.writeheader()
        for operation in operations_data:
            operation_data = Operation.from_orm(operation)
            writer.writerow(operation_data.dict())
        output.seek(0)
        return output
