from fastapi import (APIRouter, Depends, File, Request, Query,
                     UploadFile, Response, status, BackgroundTasks)
from fastapi.responses import StreamingResponse
from typing import Union
from datetime import date
from ..services.auth import get_current_user
from ..models.auth import User
from ..models.operation import OperationKind
from ..services.reports import ReportsService
from ..log import logger, log_msg


router = APIRouter(
    prefix="/reports",
    tags=["Reports"]
)


@router.post("/import")
def import_csv(
        request: Request,
        background_tasks: BackgroundTasks,
        file: UploadFile = File(),
        reports_service: ReportsService = Depends(),
        user: User = Depends(get_current_user)):
    """Imports operations from CSV file
    Args:
        request (Request): HTTP request data
        background_tasks (BackgroundTasks): Special object to run
        tasks asymchronous
        file (UploadFile, optional): CSV file. Defaults to File().
        reports_service (ReportsService, optional): Reports service object.
        Defaults to Depends().
        user (User, optional): Authentificate uer data.
        Defaults to Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Operations list imported by user "
                                  f"with id {user.id}")))
    background_tasks.add_task(
        reports_service.import_csv,
        user_id=user.id,
        file=file.file)
    return Response(status_code=status.HTTP_200_OK)


@router.get("/export")
def export_csv(
        request: Request,
        kind: Union[None, OperationKind] = Query(
            default=None,
            title="Operation kind",
            description="Kind of operation like income or outcome"
        ),
        start_date: Union[None, date] = Query(
            default=None,
            title="Start date",
            description="Date before which to filter"
        ),
        end_date: Union[None, date] = Query(
            default=None,
            title="End date",
            description="Date after which to filter"
        ),
        reports_service: ReportsService = Depends(),
        user: User = Depends(get_current_user)):
    """Exports operations to CSV file
    Args:
        request (Request): HTTP request data
        kind (Optional[OperationKind], optional): what kind of operations to
        filter. Defaults to None.
        start_date (Optional[date], optional): Start date to filter.
        Defaults to None.
        end_date (Optional[date], optional): End date to filter.
        Defaults to None.
        reports_service (ReportsService, optional): Reports service object.
        Defaults to Depends().
        user (User, optional): Authentificate uer data.
        Defaults to Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Operations list exported by user "
                                  f"with id {user.id} and following "
                                  f"filters: kind={kind}, "
                                  f"start_date={start_date}, "
                                  f"end_date={end_date}")))
    report = reports_service.export_csv(
        user_id=user.id,
        kind=kind,
        start_date=start_date,
        end_date=end_date)
    return StreamingResponse(
        report,
        media_type="text/csv",
        headers={
            "Content-Disposition": "attachment; filename=report.csv"
        }
    )
