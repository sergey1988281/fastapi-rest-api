from fastapi import APIRouter, Depends, Request, Response, status
from fastapi.security import OAuth2PasswordRequestForm
from ..models.auth import Token, UserCreate, User, UserPassword
from ..services.auth import AuthService, get_current_user
from ..log import logger, log_msg


router = APIRouter(
    prefix="/auth",
    tags=["Auth"]
)


@router.post("/sign-up", response_model=User)
def sign_up(user_data: UserCreate,
            request: Request,
            auth_service: AuthService = Depends()):
    """Regsters a new user
    Args:
        user_data (UserCreate): Data for user to create
        request (Request): HTTP request details
        auth_service (AuthService, optional): Dependency injection.
        Defaults to Depends().

    Returns:
        User: Created user details
    """
    logger.info(log_msg(request, f"Registration request {user_data.username}"))
    return auth_service.register_user(user_data)


@router.post("/sign-in", response_model=Token)
def sign_in(request: Request,
            form_data: OAuth2PasswordRequestForm = Depends(),
            auth_service: AuthService = Depends()):
    """Login registered user and issue a token
    Args:
        request (Request): HTTP request details.
        form_data (OAuth2PasswordRequestForm, optional): OAuth2 username
        and password. Defaults to Depends().
        auth_service (AuthService, optional): Dependency injection.
        Defaults to Depends().
    Returns:
        _type_: JWT token
    """
    logger.info(log_msg(request, (f"Authentification request "
                                  f"{form_data.username}")))
    return auth_service.authentificate_user(
        username=form_data.username,
        password=form_data.password
    )


@router.get("/user", response_model=User)
def get_current_user(request: Request,
                     user: User = Depends(get_current_user)):
    """Get details about user who logged in
    Args:
        request (Request): HTTP request details
        user (User, optional): Dependency injection.
        Defaults to Depends(get_current_user).
    Returns:
        User: Logged user data
    """
    logger.info(log_msg(request, (f"User {user.username} requested"
                                  f" its own details")))
    return user


@router.patch("/user", response_model=Token)
def change_password(request: Request,
                    password_data: UserPassword,
                    user: User = Depends(get_current_user),
                    auth_service: AuthService = Depends()):
    """Allows user to change passord
    Args:
        request (Request): HTTP request details
        password_data (UserPassword): New password
        user (User, optional): User to change password.
        Defaults to Depends(get_current_user).
        auth_service (AuthService, optional): Dependency injection.
        Defaults to Depends().
    Returns:
        Token: Refreshed JWT token
    """
    logger.info(log_msg(request, (f"Password update requested"
                                  f"for user {user.username}")))
    return auth_service.change_user_password(user_id=user.id,
                                             password=password_data.password)


@router.delete("/user", status_code=status.HTTP_204_NO_CONTENT)
def delete_user(request: Request,
                user: User = Depends(get_current_user),
                auth_service: AuthService = Depends()):
    """Deletes user from database
    Args:
        request (Request): HTTP request details
        user (User, optional): User to delete.
        Defaults to Depends(get_current_user).
        auth_service (AuthService, optional): Dependency injection.
        Defaults to Depends().
    """
    logger.info(log_msg(request, (f"User id {user.id} requested to delete")))
    auth_service.delete_user(user_id=user.id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
