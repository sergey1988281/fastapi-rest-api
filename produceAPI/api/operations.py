from typing import List, Union
from fastapi import APIRouter, Depends, Request, Response, status, Path, Query
from datetime import date
from ..models.operation import (Operation as Operation_model,
                                OperationCreate,
                                OperationUpdate,
                                OperationKind,
                                BaseOperation)
from ..models.auth import User
from ..services.operations import OperationService
from ..services.auth import get_current_user
from ..log import logger, log_msg


router = APIRouter(
    prefix="/operations",
    tags=["Operations"]
)


@router.get("/", response_model=List[Operation_model])
def get_operations(
        request: Request,
        kind: Union[None, OperationKind] = Query(
            default=None,
            title="Operation kind",
            description="Kind of operation like income or outcome"
        ),
        start_date: Union[None, date] = Query(
            default=None,
            title="Start date",
            description="Date before which to filter"
        ),
        end_date: Union[None, date] = Query(
            default=None,
            title="End date",
            description="Date after which to filter"
        ),
        operation_service: OperationService = Depends(),
        user: User = Depends(get_current_user)):
    """Retrieves list of operations
    Args:
        request (Request): HTTP request details
        kind (Optional[OperationKind], optional): what kind of operations to
        filter. Defaults to None.
        start_date (Optional[date], optional): Start date to filter.
        Defaults to None.
        end_date (Optional[date], optional): End date to filter.
        Defaults to None.
        operation_service (OperationService, optional): Dependency injection.
        Defaults to Depends().
        user (User, optional): Dependency injection. Defaults to
        Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Operations list requested by user with id "
                                  f"{user.id} and following "
                                  f"filters: kind={kind}, "
                                  f"start_date={start_date}, "
                                  f"end_date={end_date}")))
    return operation_service.get_operations(user_id=user.id,
                                            kind=kind,
                                            start_date=start_date,
                                            end_date=end_date)


@router.get("/{id}", response_model=Operation_model)
def get_operation_by_id(
        request: Request,
        id: int = Path(
            title="Operation ID",
            description="ID of operation to retrieve",
            ge=1
        ),
        operation_service: OperationService = Depends(),
        user: User = Depends(get_current_user)):
    """Retrieves a specific operation by id
    Args:
        request (Request): HTTP request details
        id (int): _description_
        operation_service (OperationService, optional): Dependency injection.
        Defaults to Depends().
        user (User, optional): Dependency injection. Defaults to
        Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Operation with id: {id} been requested "
                                  f"by user with id {user.id}")))
    return operation_service.get_operation(id=id, user_id=user.id)


@router.post("/", response_model=Operation_model)
def create_operation(
        request: Request,
        operation_data: OperationCreate,
        operation_service: OperationService = Depends(),
        user: User = Depends(get_current_user)):
    """Creates a new operation
    Args:
        request (Request): HTTP request details
        operation_data (OperationCreate): Details of new post to create
        operation_service (OperationService, optional): Dependency injection.
        Defaults to Depends().
        user (User, optional): Dependency injection. Defaults to
        Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Post creation requested "
                                  f"by user with id {user.id} details: "
                                  f"{operation_data.dict()}")))
    return(operation_service.create_operation(operation_data=operation_data,
                                              user_id=user.id))


@router.put("/{id}", response_model=Operation_model)
def update_operation(
        request: Request,
        operation_data: OperationUpdate,
        id: int = Path(
            title="Operation ID",
            description="ID of operation to update",
            ge=1
        ),
        operation_service: OperationService = Depends(),
        user: User = Depends(get_current_user)):
    """Full update of all operation fields by id
    Args:
        request (Request): HTTP request details
        id (int): Id of post to update
        operation_data (OperationUpdate): post update details
        operation_service (OperationService, optional): Dependency injection.
        Defaults to Depends().
        user (User, optional): Dependency injection. Defaults to
        Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Post {id} full update requested "
                                  f"by user with id {user.id} details: "
                                  f"{operation_data.dict()}")))
    return(operation_service.update_operation(id=id,
                                              user_id=user.id,
                                              operation_data=operation_data))


@router.patch("/{id}", response_model=Operation_model)
def update_operation_description(
        request: Request,
        operation_data: BaseOperation,
        id: int = Path(
            title="Operation ID",
            description="ID of operation to change description",
            ge=1
        ),
        operation_service: OperationService = Depends(),
        user: User = Depends(get_current_user)):
    """Update only operation description by id
    Args:
        request (Request): HTTP request details
        id (int): Id of post to update
        description (str, optional): new post description. Defaults to blank
        operation_service (OperationService, optional): Dependency injection.
        Defaults to Depends().
        user (User, optional): Dependency injection. Defaults to
        Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Post {id} description update requested "
                                  f"by user with id {user.id} new description:"
                                  f" {operation_data.description}")))
    return(operation_service.update_operation_description(
                                                id=id,
                                                user_id=user.id,
                                                operation_data=operation_data))


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_operation(
        request: Request,
        id: int = Path(
            title="Operation ID",
            description="ID of operation to delete",
            ge=1
        ),
        operation_service: OperationService = Depends(),
        user: User = Depends(get_current_user)):
    """Delete of operation by specified id
    Args:
        request (Request): HTTP request details
        id (int): Id of post to delete
        operation_service (OperationService, optional): Dependency injection.
        Defaults to Depends().
        user (User, optional): Dependency injection. Defaults to
        Depends(get_current_user).
    """
    logger.info(log_msg(request, (f"Post {id} delete requested"
                                  f"by user with id {user.id}")))
    operation_service.delete_operation(id=id, user_id=user.id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
