from unicodedata import name
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from .api import router
from .settings import settings


tags_metadata = [
    {
        "name": "Auth",
        "description": ("Deals with user registration, "
                        "authentification and authorization")
    },
    {
        "name": "Operations",
        "description": "Work with operations data"
    },
    {
        "name": "Reports",
        "description": "Import/Export data as file"
    }
]

app = FastAPI(
    title="FastAPI REST API example",
    description="Basic produceAPI example using FastAPI",
    version="1.0.0",
    contact={
        "name": "Sergey Kiyan",
        "email": "sergey198828@gmail.com"
    },
    license_info={
        "name": "MIT",
        "url": "https://opensource.org/licenses/MIT"
    },
    openapi_tags=tags_metadata
)
app.include_router(router)
print(__name__)
app.mount("/static",
          StaticFiles(directory=settings.static_folder),
          name="static")
templates = Jinja2Templates(directory=settings.templates_folder)


@app.get("/", response_class=HTMLResponse)
def index(request: Request):
    """_summary_
    Main page stub
    """
    title = "Welcome to FastAPI demo project"
    with open('./README', 'r') as file:
        body = file.read().replace('\n', '<br>')
    return templates.TemplateResponse("index.html",
                                      {"request": request,
                                       "title": title,
                                       "body": body})
