import uvicorn
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.exc import OperationalError, IntegrityError
from datetime import date
from time import sleep
from .services.auth import AuthService
from .settings import settings
from .database import url, engine, Session, commit_to_session, add_to_session
from .models.orm.operation import Operation
from .models.orm.user import User
from .log import logger

# Check if database is ok and create if not
if not database_exists(url):
    retry = 1
    while(retry < 4):
        try:
            create_database(url)
            logger.info(f"Database not found and just been created")
            break
        except OperationalError:
            logger.info(f"{retry} attempt to create DB")
            retry += 1
            sleep(5)
            continue
        except Exception as err:
            logger.critical(f"Unexpected error criating DB")
            logger.critical(str(err))
            raise err
try:
    Operation.metadata.create_all(engine)
    user = User(
        id=99,
        email="username@example.com",
        username="username",
        password_hash=AuthService.hash_password("Test-1234"))
    operation1 = Operation(user_id=99,
                           date=date.today(),
                           kind="income",
                           amount=1000,
                           description="Salary")
    operation2 = Operation(user_id=99,
                           date=date.today(),
                           kind="outcome",
                           amount=500,
                           description="Restraunt")
    session = Session()
    add_to_session(session, user)
    commit_to_session(session, flush=True)
    add_to_session(session, [operation1, operation2])
    commit_to_session(session)
    logger.info(f"Database been initialized with initial structure")
except IntegrityError:
    logger.info("Database seems contain initial structure already")
except Exception as err:
    logger.critical(f"Unexpected error initializing DB")
    logger.critical(str(err))
    raise err

# Run application server
logger.info("Starting application")
uvicorn.run(
    "produceAPI.app:app",
    host=settings.server_host,
    port=settings.server_port,
    log_level=settings.server_log_level
    )
