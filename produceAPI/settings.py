from pydantic import BaseSettings


class Settings(BaseSettings):
    # HTTP server settings
    server_host: str = "127.0.0.1"
    server_port: int = 8000
    server_log_level: str = "debug"
    # Database server settings
    database_user: str
    database_passwd: str
    database_host: str
    database_port: str
    database_name: str
    # HTML files settings
    static_folder: str = "produceAPI/static"
    templates_folder: str = "produceAPI/templates"
    # Logger settings
    log_file: str = "produceAPI.log"
    log_format: str = "{time} - {name} - {level} - {message}"
    # JWT settings
    jwt_secret: str
    jwt_algorithm: str = "HS256"
    jwt_expiration: int = 3600


settings = Settings(
    _env_file=".env",
    _env_file_encoding="utf-8"
)
