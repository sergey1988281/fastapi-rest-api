from typing import Iterable
from collections.abc import Iterable as Many
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .settings import settings
from .log import logger
from sqlalchemy.ext.declarative import declarative_base


url = (f"postgresql://{settings.database_user}"
       f":{settings.database_passwd}"
       f"@{settings.database_host}"
       f":{settings.database_port}"
       f"/{settings.database_name}")

engine = create_engine(url, pool_size=50, echo=False)

Session = sessionmaker(
    engine,
    autocommit=False,
    autoflush=False
)

Base = declarative_base()


def get_session() -> Session:
    """Dependency injection
    Returns:
        Session: sqlalchemy session object
    Yields:
        Iterator[Session]: sqlalchemy session object
    """
    session = Session()
    try:
        yield session
    finally:
        session.close()


def add_to_session(session: Session, object: Base | Iterable[Base]) -> None:
    """Adds given object to a given session
    Args:
        session (Session): Sesion object
        object (Base): Database entity object
    Raises:
        SQLAlchemy exception
    """
    try:
        if isinstance(object, Many):
            session.add_all(object)
        else:
            session.add(object)
    except Exception as err:
        logger.exception(str(err))
        raise


def commit_to_session(session: Session, flush: bool = False) -> None:
    """Performs commit to a given session
    Args:
        session (Session): Sesion object
    Raises:
        SQLAlchemy exception
    """
    try:
        if flush:
            session.flush()
        else:
            session.commit()
    except Exception as err:
        logger.exception(str(err))
        session.rollback()
        raise
