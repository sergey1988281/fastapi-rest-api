import re
from decimal import Decimal
from typing import Optional
from enum import Enum
from pydantic import BaseModel, validator
from datetime import date


class OperationKind(str, Enum):
    INCOME = "income"
    OUTCOME = "outcome"


class BaseOperation(BaseModel):
    description: Optional[str]

    @validator("description")
    def description_format(cls, description):
        pattern = re.compile(r'^[A-Za-z\d ]*$')
        if description is not None:
            if len(description) > 50 or not re.match(pattern, description):
                raise ValueError(f"Only leters, digits and whitespaces"
                                 f" are supported for description. Must"
                                 f" not be no longer than 50 symbols")
        return description


class ParentOperation(BaseOperation):
    date: date
    kind: OperationKind
    amount: Decimal

    @validator("amount")
    def positive_amount(cls, a):
        if a <= 0:
            raise ValueError("Amount must be positive")
        return a


class Operation(ParentOperation):
    id: int
    user_id: int

    class Config:
        orm_mode = True


class OperationCreate(ParentOperation):
    pass


class OperationUpdate(ParentOperation):
    pass
