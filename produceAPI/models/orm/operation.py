import sqlalchemy as sa
from sqlalchemy.orm import relationship
from produceAPI.database import Base


class Operation(Base):
    __tablename__ = "operations"

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("users.id",
                                                  ondelete="CASCADE"))
    date = sa.Column(sa.Date, nullable=False)
    kind = sa.Column(sa.String, nullable=False)
    amount = sa.Column(sa.Numeric(10, 2), nullable=False)
    description = sa.Column(sa.String, nullable=True)
    user = relationship(
        "User",
        back_populates="operation"
        )
