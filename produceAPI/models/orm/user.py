import sqlalchemy as sa
from sqlalchemy.orm import relationship
from produceAPI.database import Base


class User(Base):
    __tablename__ = "users"

    id = sa.Column(sa.Integer, primary_key=True)
    email = sa.Column(sa.Text, unique=True)
    username = sa.Column(sa.Text, unique=True)
    password_hash = sa.Column(sa.Text)
    operation = relationship(
        "Operation",
        back_populates="user",
        cascade="all, delete",
        passive_deletes=True
    )
