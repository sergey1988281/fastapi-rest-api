import re
from pydantic import BaseModel, EmailStr, validator
from string import ascii_lowercase, ascii_uppercase, digits, punctuation


class BaseUser(BaseModel):
    email: EmailStr
    username: str

    @validator("username")
    def username_format(cls, username):
        pattern = re.compile(r'^[A-Za-z][\w][A-Za-z\d]*$')
        if not re.match(pattern, username):
            raise ValueError(f"Only leters, digits and underscores"
                             f" are supported for username. Must"
                             f" not start from digit or underscore,"
                             f" must not end with underscore")
        return username


class UserPassword(BaseModel):
    password: str

    @validator("password")
    def password_format(cls, password):
        if (len(password) < 8 or
           not any((symbol in ascii_lowercase for symbol in password)) or
           not any((symbol in ascii_uppercase for symbol in password)) or
           not any((symbol in digits for symbol in password)) or
           not any((symbol in punctuation for symbol in password))):
            raise ValueError(f"Password policy violation "
                             f"must be minimum 8 characters "
                             f"must contain uppercase and lowercase "
                             f"must contain digit and punctuation")
        return password


class UserCreate(BaseUser, UserPassword):
    pass


class User(BaseUser):
    id: int

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str = "bearer"
