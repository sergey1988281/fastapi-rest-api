from fastapi.testclient import TestClient

from ..app import app

client = TestClient(app)


def test_appropriate_sign_up():
    headers = {
        'accept': 'application/json',
    }
    data = {
        "password": "Test-1234",
        "email": "user@example.com",
        "username": "user"
    }
    response = client.post(
            "/auth/sign-up",
            headers=headers,
            json=data
        )
    assert response.status_code == 200
    assert response.json() == {
                "email": "user@example.com",
                "username": "user",
                "id": 1
                }


def test_same_username_sign_up():
    headers = {
        'accept': 'application/json',
    }
    data = {
        "password": "Test-1234",
        "email": "anotheruser@example.com",
        "username": "user"  # Existing username
    }
    response = client.post(
            "/auth/sign-up",
            headers=headers,
            json=data
        )
    assert response.status_code == 409


def test_same_email_sign_up():
    headers = {
        'accept': 'application/json',
    }
    data = {
        "password": "Test-1234",
        "email": "user@example.com",  # Existing email
        "username": "anotheruser"
    }
    response = client.post(
            "/auth/sign-up",
            headers=headers,
            json=data
        )
    assert response.status_code == 409


def test_inappropriate_password_sign_up():
    headers = {
        'accept': 'application/json',
    }
    data = {
        "password": "Test1234",
        "email": "user@example.com",
        "username": "user"
    }
    response = client.post(
            "/auth/sign-up",
            headers=headers,
            json=data
        )
    assert response.status_code == 422
    assert response.json() == {
        "detail": [{
                    "loc": [
                        "body",
                        "password"
                    ],
                    "msg": ("Password policy violation must be minimum 8 "
                            "characters must contain uppercase and lowercase "
                            "must contain digit and punctuation"),
                    "type": "value_error"
                    }
                   ]
        }


def test_appropriate_sign_in():
    headers = {
        "content-type": "application/x-www-form-urlencoded"
        }
    data = {
        "username": "user",
        "password": "Test-1234"
        }
    response = client.post("/auth/sign-in", headers=headers, data=data)
    assert response.status_code == 200
